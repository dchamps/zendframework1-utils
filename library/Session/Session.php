<?php

/**
 * Vanguarda Brasil
 *
 * @category  Vanguarda Brasil
 * @package   Session
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   SVN: $Id$
 */

namespace Dchamps\ZendFramework1\Session;

/**
 * Class Session
 *
 * Controle de autenticação
 *
 * @category  Vanguarda Brasil
 * @package   Session
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   Release: 0.0.0
 */
class Session extends \Zend_Session
{

    private static $namespace = 'admin';

    public static function setNamespace($namespace)
    {
        self::$namespace = $namespace;
    }

    /**
     * Inicia sessão
     *
     * @param $login Model a ser serializado na sessão
     *
     * @return void
     */
    public static function login($login)
    {
        $session = new \Zend_Session_Namespace(self::$namespace);
        $session->login = serialize($login->toArray());
    }

    /**
     * Verifica se sessão está iniciada
     *
     * @return bool
     */
    public static function logged()
    {
        self::start();

        return self::namespaceIsset(self::$namespace);
    }

    /**
     * Fecha sessão
     *
     * @return void
     */
    public static function logout()
    {
        $session = new \Zend_Session_Namespace(self::$namespace);
        $session->unsetAll();

        self::namespaceUnset(self::$namespace);
    }

    public static function get()
    {
        if (self::logged()) {
            $session = new \Zend_Session_Namespace(self::$namespace);
            return unserialize($session->login);
        }

        return null;
    }

    public static function setAttribute($namespace, $key, $value)
    {
        if (self::logged()) {
            $session = new \Zend_Session_Namespace($namespace);
            $session->{$key} = $value;
        }
    }

    public static function getAttribute($namespace, $key)
    {
        if (self::logged()) {
            $session = new \Zend_Session_Namespace($namespace);
            return $session->{$key};
        }

        return null;
    }
}
