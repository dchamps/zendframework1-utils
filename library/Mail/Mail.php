<?php

/**
 * Vanguarda Brasil
 *
 * @category  Vanguarda Brasil
 * @package   Mail
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   SVN: $Id$
 */

namespace Dchamps\ZendFramework1\Mail;

/**
 * Class Mail\Mail
 *
 * @category  Vanguarda Brasil
 * @package   Mail
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   Release: 0.0.0
 */
class Mail extends \PHPMailer
{
    /**
     * Instancia PHPMailer atribuindo constantes de configurações SMTP
     */
    public function __construct($const)
    {
        parent::isSMTP();

        $this->Host       = $const['host'];
        $this->Port       = $const['port'];
        $this->SMTPSecure = $const['secure'];
        $this->SMTPAuth   = true;
        $this->Username   = $const['username'];
        $this->Password   = $const['password'];
        $this->CharSet    = 'UTF-8';
    }

    public function replyTo($address, $name)
    {
        try {
            parent::AddReplyTo($address, $name);
        } catch (phpmailerException $e) {
            echo $e->errorMessage();
        }
    }

    public function from($address, $name)
    {
        try {
            parent::setFrom($address, $name);
        } catch (phpmailerException $e) {
            echo $e->errorMessage();
        }
    }

    public function to($addresses, $name = null)
    {
        try {
            if ($this->isArray($addresses, $name)) {
                foreach ($addresses as $address => $name) {
                    parent::addAddress($address, $name);
                }
            }

            if ($this->isString($addresses)) {
                parent::addAddress($addresses, $name);
            }
        } catch (phpmailerException $e) {
            echo $e->errorMessage();
        }
    }

    public function subject($subject)
    {
        return $this->Subject = $subject;
    }

    public function body($body)
    {
        return parent::msgHTML($body);
    }

    private function isArray($addresses, $name)
    {
        return is_array($addresses) && !isset($name);
    }

    private function isString($address)
    {
        return is_string($address);
    }
}
