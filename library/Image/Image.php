<?php

/**
 * Vanguarda Brasil
 *
 * @category  Vanguarda Brasil
 * @package   Image
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   SVN: $Id$
 */

namespace Dchamps\ZendFramework1\Image;

use WideImage\WideImage;

/**
 * Class Image\Image
 *
 * Classe com definições específicas referentes ao resize de imagens
 *
 * @category  Vanguarda Brasil
 * @package   Image
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   Release: 0.0.0
 */
class Image
{
    /**
     * Método para resize de imagem
     *
     * @param $image
     *
     * @return WideImage
     */
    public static function resize($image, $width, $height, $crop = false)
    {
        $wideImage = WideImage::load($image['tmp_name']);
        $thumb = self::createThumb($wideImage, $width, $height);

        if ($crop) {
            $thumb = $thumb->crop("center", "middle", $width, $height);
        }

        return $thumb;
    }

    /**
     * Função realiza o resize da imagem mantendo as proporções
     *
     * @param WideImage $image
     * @param int       $width
     * @param int       $height
     * @param string    $mode
     *
     * @return WideImage
     */
    private static function createThumb($image, $width, $height, $mode = 'fill')
    {
        return ($image->getWidth() >= $image->getHeight())
            ? $image->resize($width, null, $mode)
            : $image->resize(null, $height, $mode);
    }
}
