<?php

namespace Dchamps\ZendFramework1\Sitemap;

use Dchamps\ZendFramework1\Sitemap\Url;

/**
 * Gerador de Sitemap
 * Baseado no php-sitemap-generator de Paweł Antczak
 *
 * @author  Thiago Henrique Poiani
 * @version 1.0.0
 * @see     https://github.com/pawelantczak/php-sitemap-generator
 * @see     http://www.sitemaps.org
 */
class Sitemap
{
    /**
     * Nome do arquivo
     * @var string
     */
    private $fileName = "sitemap.xml";

    /**
     * URL do site
     * @var string
     */
    private $baseUrl;

    /**
     * Caminho do arquivo
     * @var string
     */
    private $basePath;

    /**
     * Caminho inteiro do arquivo
     * @var string
     */
    private $fullPath;

    /**
     * Array com urls
     * @var array of strings
     */
    private $urls = array();

    /**
     * Construtor
     *
     * @param string $baseURL  URL do site
     * @param string $basePath Caminho do arquivo
     * @param string $fileName Nome do arquivo
     */
    public function __construct($baseURL, $basePath, $fileName = null)
    {
        if ($this->isNullOrEmpty($baseURL) || $this->isNullOrEmpty($basePath)) {
            throw new \InvalidArgumentException("A URL do site e o Caminho do arquivo devem ser passados por parâmetro.");
        }

        if (!$this->isDirectory($basePath)) {
            throw new \InvalidArgumentException("O Caminho do arquivo devem referenciar uma pasta.");
        }

        $this->baseURL = $baseURL;
        $this->basePath = $this->pathValidation($basePath);

        if ($fileName) {
            $this->fileName = $fileName;
        }

        $this->fullPath = $this->basePath . DIRECTORY_SEPARATOR . $this->fileName;

        if (!$this->isWritable($this->fullPath)) {
            throw new \Exception("Arquivo {$this->fileName} não encontrado ou não pode ser sobrescrito.");
        }
    }

    /**
     * Use this to add single URL to sitemap.
     *
     * @param string $loc             URL
     * @param string $lastModified    When it was modified, use ISO 8601
     * @param string $changeFrequency How often search engines should revisit this URL
     * @param string $priority        Priority of URL on You site
     */
    public function addUrl($loc, $lastModified = null, $changeFrequency = null, $priority = null)
    {
        if ($this->isNullOrEmpty($loc)) {
            throw new \InvalidArgumentException("URL devem ser passada por parâmetro.");
        }

        $url = new Url($loc, $lastModified, $changeFrequency, $priority);
        array_push($this->urls, $url);
    }

    /**
     * Método para criar sitemap em memória
     */
    public function createSitemap()
    {
        if (!$this->hasUrl($this->urls)) {
            throw new \Exception("Nenhuma Url adicionada para criação de sitemap.");
        }

        $header = '<?xml version="1.0" encoding="UTF-8"?>
                   <urlset
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                       http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
                       xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                   </urlset>';

        $xml = new \SimpleXMLElement($header);
        foreach ($this->urls as $url) {
            $this->addNode($xml, $url);
        }

        $this->writeSitemap($xml);
    }

    /**
     * Método para salvar sitemap
     *
     * @param SimpleXMLElement $xml
     */
    private function writeSitemap(\SimpleXMLElement $xml)
    {
        $file = fopen($this->fullPath, 'w');
        fwrite($file, $xml->asXML());

        return fclose($file);
    }

    /**
     * @param string $sitemapFileName
     */
    public function setSitemapFileName($sitemapFileName)
    {
        $this->fileName = $sitemapFileName;
    }

    /**
     * @return string
     */
    public function getSitemapFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $basePath
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * @return string
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * Método para verificação de string nula ou vazia
     *
     * @param string $string
     *
     * @return bool
     */
    private function isNullOrEmpty($string)
    {
        return (!isset($string) || trim($string) === '');
    }

    /**
     * Método para retirar DIRECTORY_SEPARATOR do final do caminho
     *
     * @param string $basePath
     *
     * @return string
     */
    private function pathValidation($basePath)
    {
        return rtrim($basePath, DIRECTORY_SEPARATOR);
    }

    /**
     * Método para verificar se há Url na lista
     *
     * @param array $url
     *
     * @return bool
     */
    private function hasUrl($url)
    {
        return is_array($url) && !empty($url);
    }

    /**
     * Método para verificar se o diretório existe
     *
     * @param string $path
     *
     * @return bool
     */
    private function isDirectory($path)
    {
        return is_dir($path);
    }

    /**
     * Método para verificar se o arquivo pode ser sobrescrito
     *
     * @param string $file
     *
     * @return bool
     */
    private function isWritable($file)
    {
        return is_writable($file);
    }

    /**
     * Método para adicionar nó com dados da URL ao XML
     *
     * @param             $xml
     * @param Gerator\Url $url
     */
    private function addNode($xml, Url $url)
    {
        $row = $xml->addChild('url');

        $loc = $url->getLoc();
        $lastModified = $url->getLastModified();
        $changeFrequency = $url->getChangeFrequency();
        $priority = $url->getPriority();

        $row->addChild('loc', htmlspecialchars($loc, ENT_QUOTES, 'UTF-8'));

        if (!$this->isNullOrEmpty($lastModified)) {
            $row->addChild('lastmod', $lastModified);
        }

        if (!$this->isNullOrEmpty($changeFrequency)) {
            $row->addChild('changefreq', $changeFrequency);
        }

        if (!$this->isNullOrEmpty($priority)) {
            $row->addChild('priority', $priority);
        }
    }
}
