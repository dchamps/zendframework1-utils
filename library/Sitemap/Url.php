<?php

namespace Dchamps\ZendFramework1\Sitemap;

/**
 * Gerador de Sitemap
 * Baseado no php-sitemap-generator de Paweł Antczak
 *
 * @author  Thiago Henrique Poiani
 * @version 1.0.0
 * @see     https://github.com/pawelantczak/php-sitemap-generator
 * @see     http://www.sitemaps.org
 */
class Url
{
    private $loc;
    private $lastModified;
    private $changeFrequency;
    private $priority;

    public function __construct($loc, $lastmod = null, $changefreq = null, $priority = null)
    {
        if (!$this->isValid($loc)) {
            throw new \InvalidArgumentException("O tamanho da URL não pode ser maior que 2048 caracteres.");
        }

        $this->loc = $loc;

        if ($lastmod) {
            $this->lastModified = $lastmod;
        }

        if ($changefreq) {
            $this->changeFrequency = $changefreq;
        }

        if ($priority) {
            $this->priority = $priority;
        }
    }

    /**
     * @return mixed
     */
    public function getLoc()
    {
        return $this->loc;
    }

    /**
     * @return mixed
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * @return mixed
     */
    public function getChangeFrequency()
    {
        return $this->changeFrequency;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Método para retornar se a URL é válida
     *
     * @param $url
     *
     * @return bool
     */
    private function isValid($url)
    {
        $urlLenght = extension_loaded('mbstring') ? mb_strlen($url) : strlen($url);

        return $urlLenght <= 2048;
    }
}
