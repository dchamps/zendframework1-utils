<?php

namespace Dchamps\ZendFramework1\Sitemap;

/**
 * Gerador de Sitemap
 * Baseado no php-sitemap-generator de Paweł Antczak
 *
 * @author  Thiago Henrique Poiani
 * @version 1.0.0
 * @see     https://github.com/pawelantczak/php-sitemap-generator
 * @see     http://www.sitemaps.org
 */
abstract class Frequency
{
    const ALWAYS  = 'always';
    const HOURLY  = 'hourly';
    const DAILY   = 'daily';
    const WEEKLY  = 'weekly';
    const MONTHLY = 'monthly';
    const YEARLY  = 'yearly';
    const NEVER   = 'never';
}
