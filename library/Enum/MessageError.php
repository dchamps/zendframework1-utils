<?php

/**
 * Vanguarda Brasil
 *
 * @category  Vanguarda Brasil
 * @package   Enum
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   SVN: $Id$
 */

namespace Dchamps\ZendFramework1\Enum;

/**
 * Class Enum\MessageError
 *
 * @category  Vanguarda Brasil
 * @package   Enum
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   Release: 0.0.0
 */
abstract class MessageError
{
    const SUCCESS = 'success';
    const ERROR = 'error';
}
