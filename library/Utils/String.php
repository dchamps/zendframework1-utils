<?php

/**
 * Vanguarda Brasil
 *
 * @category  Vanguarda Brasil
 * @package   Utils
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   SVN: $Id$
 */

namespace Dchamps\ZendFramework1\Utils;

use Cocur\Slugify\Slugify;

/**
 * Class Dchamps\ZendFramework1\Utils\IO
 *
 * Classe com definições específicas referentes a strings
 *
 * @category  Vanguarda Brasil
 * @package   Utils
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   Release: 0.0.0
 */
class String
{

    /**
     * Verifica se a string é nula ou vazia
     *
     * @param string $string String a ser verificada
     *
     * @return bool
     */
    public static function isNullOrEmpty($string)
    {
        return (!isset($string) || trim($string) === '');
    }

    /**
     * Verifica se a string começa com um caracter específico
     *
     * @param string $string String a ser verificada
     * @param char   $char   Caractere a ser verificado
     *
     * @return bool|null
     */
    public static function startsWith($string, $char)
    {
        if (!self::isNullOrEmpty($string)
            && !self::isNullOrEmpty($char)
            && strlen($char) == 1
        ) {
            return $string[0] == $char;
        }

        return null;
    }

    /**
     * Transforma o primeiro caracter e cada um seguido de underscore em maiúsculo
     *
     * @param string $attribute String a ser verificada
     *
     * @return string
     */
    public static function upper($attribute)
    {
        if (strpos($attribute, '_')) {
            $callback = function ($word) {
                return ucfirst($word);
            };

            return implode('', array_map($callback, explode('_', $attribute)));
        }

        return ucfirst($attribute);
    }

    /**
     * Remove caracteres especiais espaços
     *
     * @param string $string String a ser verificada
     *
     * @return string
     */
    public static function clean($string)
    {
        if (!self::isNullOrEmpty($string)) {
            $slugify = new Slugify();
            return $slugify->slugify($string);
        }

        return $string;
    }
}
