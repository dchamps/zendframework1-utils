<?php

/**
 * Vanguarda Brasil
 *
 * @category  Vanguarda Brasil
 * @package   Utils
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   SVN: $Id$
 */

namespace Dchamps\ZendFramework1\Utils;

/**
 * Class Dchamps\ZendFramework1\Utils\Date
 *
 * Classe com definições específicas referentes a Date
 *
 * @category  Vanguarda Brasil
 * @package   Utils
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   Release: 0.0.0
 */
class Date
{
    /**
     * Retorna a data com o formato ANO-MES-DIA HORA:MINUTO:SEGUNDO
     *
     * @return string
     */
    public static function getCurrentTimestamp()
    {
        $date = new \DateTime();

        return $date->format("Y-m-d H:i:s");
    }

    /**
     * Retorna a data com o formato ANO-MES-DIA-HORA-MINUTO-SEGUNDO
     *
     * @return string
     */
    public static function getImageDate()
    {
        $date = new \DateTime();

        return $date->format("Y-m-d-H-i-s");
    }

    /**
     * Retorna a data com o formato específico
     * @param  string $date
     * @param  string $input
     * @param  string $output
     * @return string
     */
    public static function getDate($date, $input = 'Y-m-d', $output = 'd/m/Y')
    {
        $datetime = \DateTime::createFromFormat($input, $date);

        if ($datetime) {
            return $datetime->format($output);
        }

        return $date;
    }
}
