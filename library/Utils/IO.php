<?php

/**
 * Vanguarda Brasil
 *
 * @category  Vanguarda Brasil
 * @package   Utils
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   SVN: $Id$
 */

namespace Dchamps\ZendFramework1\Utils;

/**
 * Class Dchamps\ZendFramework1\Utils\IO
 *
 * Classe com definições específicas referentes ao sistema
 *
 * @category  Vanguarda Brasil
 * @package   Utils
 * @author    Thiago Henrique Poiani <t.poiani@vanguardabrasil.com>
 * @copyright 2007-2014 Vanguarda Brasil <http://vanguardabrasil.com.br>
 * @version   Release: 0.0.0
 */
class IO
{
    /**
     * Manipula arquivo para salvar e remover antigo, se existir
     *
     * @param  array  $source   Arquivo recuperado de $_FILES
     * @param  string $path     Caminho absoluto
     * @param  string $old_path Antigo caminho absoluto para remoção
     * @return void
     */
    public static function upload($source, $path, $old_path = null)
    {
        if (!is_array($source) || empty($source)) {
            throw new InvalidArgumentException('Arquivo não deve ser vazio.');
        }

        if (!$path) {
            throw new InvalidArgumentException('Caminho absoluto não deve ser vazio.');
        }

        $name = $path . '/' . $source['name'];

        self::mkdir($path);

        if (self::save($source, $name)) {
            if ($old_path) {
                self::unlink($old_path);
            }
        }
    }

    /**
     * Cria pasta
     *
     * @param string $path Caminho para pasta
     */
    public static function mkdir($path, $permission = 0775)
    {
        if (!is_dir($path)) {
            mkdir($path, $permission, true);
            chmod($path, $permission);
        }
    }

    /**
     * Remove arquivo
     *
     * @param string $file Caminho para arquivo
     */
    public static function unlink($file)
    {
        if (file_exists($file) && is_file($file)) {
            unlink($file);
        }
    }

    /**
     * Realiza upload de arquivo
     *
     * @param string $source Arquivo recuperado de $_FILES
     * @param string $path   Caminho do arquivo, com seu nome
     *
     * @return bool
     */
    private static function save($source, $path)
    {
        return move_uploaded_file($source['tmp_name'], $path);
    }
}
